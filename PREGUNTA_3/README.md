# Pregunta 3 Paso a paso de la solución :

## Desplegar contenedor Mysql

Para desplegar este contenedor mysql es necesario ejecutar el siguiente comando
También es necesario tener docker instalado con WSL2 si se esta utilizando windows

`docker run --name mysql-prueba3 -e MYSQL_ROOT_PASSWORD="root" -e MYSQL_USER="Zippy" -e MSQL_PASSWORD="Zippy12345" -d mysql:latest`

El comando docker run inicia la imagen , las flags -e son configuraciones como el password root, el password del user Zippy y la creación del usuario Zippy, se usa la última version de mysql disponible con el flag "-d mysql:latest".


## Probar conexión con phpMyAdmin.

Se debe ejecutar el siguiente comando

`docker run --name prueba3-phpmyadmin -v phpmyadmin-volume:/etc/phpmyadmin/config.user.inc.php --link mysql-prueba3:db -p 82:80 -d phpmyadmin/phpmyadmin`

El cual descarga y ejecuta un contenedor de phpMyAdmin y lo enlaza con la base de datos *mysql-prueba3*.

## Comprobar Conexión local a BD

Abrir el navegador en http://localhost:82 e iniciar sesión con el usuario y contraseña creados.
también se puede  iniciar con usuario root y contraseña 'root'.

phpMyAdmin permite ejecutar querys SQL, crear modificar y eliminar tablas con una interfaz visual.







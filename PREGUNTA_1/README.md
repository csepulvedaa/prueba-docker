# Pregunta 1 Paso a paso de la solución :

## Archivo HTML

Se crea un archivo index.html como sigue:
```
     <html>

<head>
    <meta charset="UTF-8">
    <title>Pregunta 1</title>
</head>

<body>
    <h1>¡Hello Zippy!</h1>
</body>

</html>
```

El cual despliega el texto ¡Hello Zippy! al ser abierto por el navegador.

## Dockerfile

El Dockerfile funciona de la siguiente manera.
Primero descarga la última versión de la imagen de nginx.
Posteriormente copia el archivo index.html a la carpeta del servidor web de ngix.

## Construir una imagen

Para construir esta imágen docker, es necesario ejecutar el siguiente comando con una consola en el directorio *PREGUNTA_1*.
También es necesario tener docker instalado con WSL2 si se esta utilizando windows

`docker image build -t pregunta-1 .`

El comando docker image build construye la imágen, el flag *-t* indica el nombre de la imágen, el (.) es el path actual.

## Arrancar un contenedor a partir de esta imágen.

Se debe ejecutar el siguiente comando

`docker container run --rm -it -p 8080:80 --name webserver-pregunta1 pregunta-1`

El comando docker container run arranca el contenedor, el flag *--rm*  es para borrar automaticamente el contenedor de los contenedores creados en caso de que este se detenga.

El flag *-it*  mantener la entrada estandar STDIN abierta y una TTY para conectarse al contenedor.

El flag *-p* indica que debe ser accedido del puerto 8080 en mi servidor local y en el puerto 80 en el contenedor, los demas flags son el nombre del contenedor *webserver-pregunta1* y el nombre de la imágen *pregunta-1*

## Comprobar Solución

Abrir el navegador en http://localhost:808 y comprobar el despliegue del texto pedido









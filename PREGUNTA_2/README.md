# Pregunta 2 Paso a paso de la solución :

## Archivo HTML

Se usa el mismo archivo HTML de la pregunta anterior

## Dockerfile

Se usa el mismo dockerfile de la pregunta anterior

## Construir una imagen

Para construir esta imágen docker, es necesario ejecutar el siguiente comando con una consola en el directorio *PREGUNTA_2*.
También es necesario tener docker instalado con WSL2 si se esta utilizando windows

`docker image build -t pregunta-2 .`

El comando docker image build construye la imágen, el flag *-t* indica el nombre de la imágen, el (.) es el path actual.

## Arrancar un contenedor a partir de esta imágen.

Se debe ejecutar el siguiente comando

`docker container run --rm -it -p 8080:80 --name webserver-pregunta2 -v ${PWD}:/usr/share/nginx/html pregunta-2 `

El comando docker container run arranca el contenedor, el flag *--rm*  es para borrar automaticamente el contenedor de los contenedores creados en caso de que este se detenga.

El flag *-it*  mantener la entrada estandar STDIN abierta y una TTY para conectarse al contenedor.

El flag *-p* indica que debe ser accedido del puerto 8080 en mi servidor local y en el puerto 80 en el contenedor, los demas flags son el nombre del contenedor *webserver-pregunta2* y el nombre de la imágen *pregunta-2*

El flag -v mapea el directorio actual al directorio del servidor utilizando volumenes compartidos, reflejando automáticamente los cambios en index.html al momento de actualizar un archivo y recargar el navegador

## Comprobar Solución

Abrir el navegador en http://localhost:808 y comprobar el despliegue del texto pedido

Editar el archivo index.html

Recargar la página








